# Apidoc Generator

Genera la documentación de un servicio web RESTFull con soporte para ApidocJS y Swagger

La documentación se genera a partir de las respuestas que devuelve el servicio.

## Requisitos:

- Es posible documentar las rutas a partir de una instancia de `express`, sin necesidad de levantar el servicio.
- Si el servicio está activo en modo `development` o `test`, es posible ejecutar funciones adicionales realizando peticiones sobre el servicio, permitiendo de esa forma documentar las respuestas automaticamente.

### Archivo `src/auth.js`

```js
const ApiGen = require('apidoc-generator')

module.exports = async () => {
  await ApiGen.post('/api/auth/signin').inputData({ body: { user: 'demo1', password: 'Developer' } }).generate()               // Sin verificar
  await ApiGen.post('/api/auth/signin').inputData({ body: { user: 'demo1', password: 'Developer' } }).request(true).generate() // Verifica la ruta
}
```

La propiedad `request(true)` indica si se va a ejecutar la petición, en tal caso se documenta el resultado de la petición automáticamente.

### Archivo `src/api.js`

```js
const ApiGen = require('apidoc-generator')

module.exports = async () => {
  const login = await ApiGen.post('/api/auth/signin').inputData({ body: { user: 'demo1', password: 'Developer' } }).execute()

  const AUTH_HEADER = { Authorization: `Bearer ${login.token}` }

  await ApiGen.get('/api/v1/instituciones').inputData({ headers: AUTH_HEADER }).request(true).generate('CUSTOM GROUP')
}
```

## Modo de uso

`node index.js`

El resultado se encuentra en la carpeta `build`

- Apidoc compilado: `build/apidoc`
- Json Swagger: `build/apidoc/swagger.json`

## Documentación

### Métodos soportados:

```js
await ApiGen.get('api/v1/users').generate()
await ApiGen.post('api/v1/users').generate()
await ApiGen.put('api/v1/users/:id').generate()
await ApiGen.patch('api/v1/users/:id').generate()
await ApiGen.delete('api/v1/users/:id').generate()
```

### Funciones disponibles:

| Función          | Descripción                                                            | Valor por defecto       |
| ---------------- | ---------------------------------------------------------------------- | ----------------------- |
| `name`           | Nombre con el que se identificará a la ruta.                           | `<method>/<path></key>` |
| `group`          | Grupo al que pertenece la ruta.                                        | `<fileName>`            |
| `description`    | Descripción de la ruta.                                                | `null`                  |
| `version`        | Versión.                                                               | `1`                     |
| `permissions`    | Lista de los roles. Ej.: `.permissions(['admin', 'user'])`             | `null`                  |
| `request`        | Indica si se va a ejecutar la petición para crear los datos de salida. | `false`                 |
| `key`            | Palabra clave que se adiciona al final del nombre de la ruta.          | `null`                  |
| `inputData`      | Datos de entrada: `{ headers: {}, params: {}, query: {}, body: {} }`   | `{}`                    |
| `outputData`     | Datos de salida: `body`                                                | `{}`                    |
| `inputExamples`  | Ejemplos de datos de entrada: `{ title: '', data: obj }`               | `null`                  |
| `outputExamples` | Ejemplos de datos de salida: `{ title: '', data: obj }`                | `null`                  |

**Ejemplo:**

```js
await ApiGen.get('/api/v1/users').generate()
await ApiGen.post('/api/v1/users').inputData({ body; { user: 'admin', pass: '123'} }).generate()
await ApiGen.post('/api/v1/users').inputData({ body; { user: 'admin', pass: '123'} }).name('Autenticar').generate()

await ApiGen.get('/api/v1/users').key('Admin').generate()
await ApiGen.get('/api/v1/users').key('User').generate()

await ApiGen.get('/ruta/verificada').request(true).generate()
```

### Función `execute`

Ejecuta la petición y devuelve el resultado del body.

```js
// Devuelve una lista de usuarios
const usuarios = await ApiGen.get('/api/v1/users').execute()
```

### Función `generate`

Ejecuta la petición (solamente si la propiedad `request` es igual a `true`), genera el respectivo apidoc y devuelve el resultado del body.

Adicionalmente se puede pasar como parámetro el nombre del grupo al que pertenece la ruta. Por defecto es el nombre del fichero.

```js
// group = <fileName>
await ApiGen.get('/api/v1/users').generate()

// group = Auth
await ApiGen.get('/api/v1/users').generate('Auth')

// Devuelve el resultado de la petición (body)
const body = await ApiGen.get('/api/v1/users').generate()
```

## Configuración del proyecto para generar la documentación


### Estructura

```txt
app
  ├─ documentation
  |    ├─ build
  |    ├─ src
  |    │    ├─ api.js
  |    │    └─ auth.js
  |    ├─ scaffold.js
  |    ├─ generate.js
  |    └─ server.js
  ├─ src
  │    └─ app.js
  ├─ index.js
  └─ package.json
```

### Archivo `documentation/scaffold.js`
Este fichero se encarga de crear los ficheros necesarios para documentar las rutas:

-  `documentation/src/api-v1-users.js`
-  `documentation/src/api-v1-users.json`
-  `documentation/src/api-v1-users.yml`

En este ejemplo, se dará prioridad al fichero `api-v1-users.js`, posteriormente al fichero `JSON` o `YAML` según se haya configurado en la variable `HELP_TYPE`.

```js
const ApiGen = require('apidoc-generator')
const path   = require('path')

ApiGen.SRC_PATH = path.resolve(__dirname, 'src')
ApiGen.HELP_TYPE = 'YAML' // YAML o JSON

const app = require('../src/app')

ApiGen.scaffold(app)
```

### Archivo `documentation/generate.js`
Este fichero se encarga de generar la documentación con soporte para ApidocJS y Swagger.

```js
const ApiGen = require('apidoc-generator')
const path   = require('path')

ApiGen.API_URL         = 'http://localhost:4000'
ApiGen.DOC_SERVER_PORT = 5000
ApiGen.BUILD_PATH      = path.resolve(__dirname, 'build')
ApiGen.SRC_PATH        = path.resolve(__dirname, 'src')
ApiGen.HELP_TYPE       = 'YAML'  // YAML o JSON
ApiGen.DESCRIPTION     = 'Descripción general del Apidoc'

ApiGen.create().catch(e => console.log(e))
```

### Archivo `documentation/server.js`
Este fichero, se encarga de ejecutar un servidor para visualizar la documentación.

```js
const ApiGen = require('apidoc-generator')
const path   = require('path')

ApiGen.DOC_SERVER_PORT = 5000
ApiGen.BUILD_PATH      = path.resolve(__dirname, 'build')

const SWAGGER_JSON_URL = `http://localhost:${ApiGen.DOC_SERVER_PORT}/swagger.json`
ApiGen.REDIRECT_PATH   = `/swagger?url=${SWAGGER_JSON_URL}`

ApiGen.server()
```

### Archivo `package.json`

```json
{
  "name": "example",
  "version": "1.0.0",
  "description": "",
  "main": "index.js",
  "scripts": {
    "start": "node index",
    "doc:scaffold": "node documentation/scaffold",
    "doc:generate": "node documentation/generate",
    "doc:server": "node documentation/server",
    "doc:start": "npm run doc:scaffold && npm run doc:generate && npm run doc:server"
  }
}
```
