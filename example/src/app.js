const express    = require('express')
const bodyParser = require('body-parser')
const cors       = require('cors')

const app = express()

app.use(bodyParser.json())
app.use(cors({
  'origin'                       : '*',
  'methods'                      : 'GET,POST,PUT,PATCH,DELETE,OPTIONS',
  'preflightContinue'            : true,
  'Access-Control-Allow-Headers' : '*'
}))

app.use(express.static('public'))

app.use((req, res, next) => {
  console.log(` [${req.method}] ${req.url}`);
  next()
})

const ROUTER1 = express.Router()

ROUTER1.get('/', (req, res, next) => {
  const users = [
    { id: 1, name: 'John', user: 'admin', pass: '123' },
    { id: 2, name: 'Dany', user: 'user', pass: '123' }
  ]
  res.status(200).json(users)
})

ROUTER1.get('/:id', (req, res, next) => {
  const user = { id: req.params.id, name: 'John', user: 'admin', pass: '123' }
  res.status(200).json(user)
})

ROUTER1.post('/', (req, res, next) => {
  const result = {
    finalizado: true,
    datos: { id: 100, name: req.body.name, user: req.body.user, pass: req.body.pass }
  }
  res.status(201).json(result)
})

// ROUTER1.post('/nueva/ruta1', (req, res, next) => { res.status(201).json({}) })
//
// ROUTER1.post('/nueva/ruta2', (req, res, next) => { res.status(201).json({}) })

ROUTER1.post('/bulk', (req, res, next) => {
  try {
    const users = req.body
    let cnt = 1
    users.forEach(user => { user.id = cnt++ })
    const result = {
      finalizado: true,
      datos: users
    }
    res.status(201).json(result)
  } catch (e) {
    res.status(500).json({ msg: 'Error' })
  }
})

const ROUTER2 = express.Router()

ROUTER2.get('/other/route', (req, res, next) => { res.status(200).json({ msg: 'ok' }) })

app.get('/ruta/sin/grupo', (req, res, next) => { res.status(200).json({ msg: 'ok' }) })
app.use('/api/v1/users', ROUTER1)
app.use('/api/custom', ROUTER2)

const PORT = 4000

app.listen(PORT)
console.log(`\n App listening on port ${PORT}\n`)

module.exports = app
