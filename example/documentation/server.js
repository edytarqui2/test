const ApiGen = require('./../../')
const path   = require('path')

ApiGen.DOC_SERVER_PORT = process.env.DOC_SERVER_PORT || 5000
ApiGen.BUILD_PATH      = path.resolve(__dirname, 'build')

const SWAGGER_JSON_URL = `http://localhost:${ApiGen.DOC_SERVER_PORT}/swagger.json`
ApiGen.REDIRECT_PATH   = `/swagger?url=${SWAGGER_JSON_URL}`

ApiGen.server()
