const ApiGen = require('./../../')
const path   = require('path')

ApiGen.SRC_PATH = path.resolve(__dirname, 'src')
ApiGen.HELP_TYPE = 'YAML' // YAML o JSON

const app = require('../src/app')

ApiGen.scaffold(app)
