const ApiGen = require('../../../')

module.exports = async () => {
  await ApiGen.get('/api/v1/users/').generate()

  await ApiGen.get('/api/v1/users/:id').generate()

  await ApiGen.post('/api/v1/users/').generate()

  await ApiGen.post('/api/v1/users/bulk').generate()

  // <!-- [ROUTE DEFINITION] --!> //
}
