const ApiGen = require('../../../')

module.exports = async () => {
  await ApiGen.get('/api/custom/other/route').generate()

  // <!-- [ROUTE DEFINITION] --!> //
}
