const ApiGen = require('./../../')
const path   = require('path')

ApiGen.API_URL         = process.env.API_URL         || 'http://localhost:4000'
ApiGen.DOC_SERVER_PORT = process.env.DOC_SERVER_PORT || 5000

ApiGen.BUILD_PATH = path.resolve(__dirname, 'build')
ApiGen.SRC_PATH   = path.resolve(__dirname, 'src')
ApiGen.HELP_TYPE  = 'YAML' // YAML o JSON

const SWAGGER_JSON_URL = `http://localhost:${ApiGen.DOC_SERVER_PORT}/swagger.json`

let description = ''
description +=  `Swagger: http://localhost:${ApiGen.DOC_SERVER_PORT}/swagger?url=${SWAGGER_JSON_URL}\n`
description +=  `ApidocJS: http://localhost:${ApiGen.DOC_SERVER_PORT}/apidoc\n`

ApiGen.DESCRIPTION = description

ApiGen.create().catch(e => console.log(e))
