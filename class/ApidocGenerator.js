const express = require('express')
const cors    = require('cors')
const request = require('request')
const fs      = require('fs')
const _       = require('lodash')
const path    = require('path')
const YAML    = require('yamljs');

const Route         = require('./Route')
const ApidocCreator = require('./ApidocCreator')
const Field         = require('./FieldCreator')

const util = require('../tools/util')

class ApidocGenerator {
  constructor () {
    this.route = new Route()
  }

  static server () {
    const app = express()
    app.use(cors({
      'origin'                       : '*',
      'methods'                      : 'GET,POST,PUT,PATCH,DELETE,OPTIONS',
      'preflightContinue'            : true,
      'Access-Control-Allow-Headers' : '*'
    }))
    app.use(express.static(path.resolve(__dirname, `${ApidocGenerator.BUILD_PATH}/public`)))
    if (ApidocGenerator.REDIRECT_PATH) {
      app.get('/', (req, res, next) => {
        res.redirect(ApidocGenerator.REDIRECT_PATH)
      })
    }
    app.listen(ApidocGenerator.DOC_SERVER_PORT, () => {
      console.log(`\n \x1b[32minfo:\x1b[0m Servidor para la documentación activo: http://localhost:${ApidocGenerator.DOC_SERVER_PORT}\n`)
    })
  }

  static scaffold (app) {
    console.log(' Apidoc Scaffolding ...\n')
    _crearCarpeta(ApidocGenerator.SRC_PATH)
    const ROUTES = _getRoutes(app)
    // console.log("ROUTES = ", ROUTES);
    const archivosVerificados = []
    Object.keys(ROUTES).forEach(groupName => {
      ROUTES[groupName].forEach(route => {
        _crearArchivos(route.groupKey)
        const filePath = path.resolve(ApidocGenerator.SRC_PATH, `${route.groupKey}.js`)
        if (!archivosVerificados.includes(filePath)) {
          _verificarRutasDelArchivo(filePath, ROUTES)
          archivosVerificados.push(filePath)
        }
        _crearContenido(route.groupKey, route)
      })
    })
    console.log('\n \x1b[32minfo:\x1b[0m Scaffold ok :) \n')
    process.exit(0)
  }

  static async create (filesName) {
    console.log(' Construyendo apidoc ...')
    const archivosConError = []
    _crearCarpeta(ApidocGenerator.BUILD_PATH)

    const outputRoutesPath = path.resolve(ApidocGenerator.BUILD_PATH, 'routes')
    _crearCarpeta(outputRoutesPath, { force: true })

    filesName = filesName || util.toArray(util.find(ApidocGenerator.SRC_PATH, '.js'), 'fileName')
    for (let i in filesName) {
      const groupKey = filesName[i]
      const fileName = `${groupKey}.js`
      const filePath = path.resolve(ApidocGenerator.SRC_PATH, fileName)
      // console.log(`\n Procesando archivo "${filePath.replace(process.cwd(), '')}" ...`)
      const dirname       = path.dirname(filePath)
      const outputDirPath = path.resolve(ApidocGenerator.BUILD_PATH, `routes/${groupKey}`)
      _crearCarpeta(outputDirPath, { force: true })
      ApidocGenerator.GROUP_NAME       = _kebabToWords(groupKey)
      ApidocGenerator.OUTPUT_TEMP_PATH = outputDirPath
      try {
        await require(filePath)()
      } catch (e) {
        console.log(` \x1b[31mError:\x1b[0m Archivo "${filePath.replace(process.cwd(), '')}"\n`)
        console.log(e)
        archivosConError.push(filePath)
      }
    }

    // Unión de ficheros y construcción del apidoc
    const outputApidocPath = path.resolve(ApidocGenerator.BUILD_PATH, 'public')
    _crearCarpeta(outputApidocPath)

    await _build()
    util.rmdir(outputRoutesPath)
    util.removeFile(path.resolve(ApidocGenerator.BUILD_PATH, 'apidoc.js'))
    util.removeFile(path.resolve(ApidocGenerator.BUILD_PATH, 'swagger.json'))

    if (archivosConError.length > 0) {
      console.log(' Los siguientes archivos, no pudieron ser procesados:\n')
      console.log(archivosConError)
      console.log('\n Verifique que el servicio a documentar se encuentre activo.\n')
      console.log('\n \x1b[31minfo:\x1b[0m Apidoc generado con algunos errores :(\n')
    } else {
      console.log('\n \x1b[32minfo:\x1b[0m Apidoc generado exitosamente :)\n')
    }
  }

  static get (routePath) { return ApidocGenerator._createRoute('get', routePath) }
  static post (routePath) { return ApidocGenerator._createRoute('post', routePath) }
  static put (routePath) { return ApidocGenerator._createRoute('put', routePath) }
  static patch (routePath) { return ApidocGenerator._createRoute('patch', routePath) }
  static delete (routePath) { return ApidocGenerator._createRoute('delete', routePath) }

  static _createRoute (method, routePath) {
    const apiGen = new ApidocGenerator()
    apiGen.route.setMethod(method)
    apiGen.route.setPath(routePath)
    return apiGen
  }

  name (name) {
    this.route.setName(name)
    return this
  }

  group (groupName) {
    this.route.setGroup(groupName)
    return this
  }

  description (description) {
    this.route.setDescription(description)
    return this
  }

  version (version) {
    this.route.setVersion(version)
    return this
  }

  permissions (permissions) {
    this.route.setPermissions(permissions)
    return this
  }

  request (value) {
    this.route.setRequest(value === true)
    return this
  }

  key (value) {
    this.route.setKey(value)
    return this
  }

  inputData (inputData) {
    this.route.setInputData(inputData)
    return this
  }

  outputData (outputData) {
    this.route.setOutputData(outputData)
    return this
  }

  inputExamples (inputExamples) {
    this.route.inputExamples = inputExamples
    return this
  }

  outputExamples (outputExamples) {
    this.route.outputExamples = outputExamples
    return this
  }

  async generate () {
    this.route.setGroup(ApidocGenerator.GROUP_NAME)
    await this.route.updateProperties({ apiUrl: ApidocGenerator.API_URL, properties: _getPropertiesExtra(this.route) })

    let apidocContent = this._getApidocContent()
    let swaggerContent = this._getSwaggerContent()

    const apiRouter = ApidocCreator.router(route => {
      // APIDOC JS
      const newBlock = route.apidoc
      const newRouteKey = newBlock.split('\n')[2]
      if (apidocContent.includes(newRouteKey)) {
        console.log(` \x1b[33m${_.padEnd(`[${route.method}]`, 9)} ${route.path} - ${route.group} (${route.name}) \u2715 Ya ha sido documentado\x1b[0m`)
      } else {
        apidocContent += newBlock
      }

      // SWAGER
      swaggerContent.paths[route.apidocSwagger.path] = swaggerContent.paths[route.apidocSwagger.path] || {}
      if (swaggerContent.paths[route.apidocSwagger.path][route.apidocSwagger.method]) {
        return
      }
      if (this.route.request) { route.apidocSwagger.content.summary = '<<< VERIFICADO >>>' }
      swaggerContent.paths[route.apidocSwagger.path][route.apidocSwagger.method] = route.apidocSwagger.content
    })
    apiRouter[this.route.method](this.route.path, this.route)

    this._generateApidocJS(apidocContent)
    this._generateSwagger(swaggerContent)

    return this.route.outputData
  }

  async execute () {
    this.route.setRequest(true)
    await this.route.updateProperties({ apiUrl: ApidocGenerator.API_URL })
    return this.route.outputData
  }

  _getApidocContent () {
    const filePath = path.resolve(ApidocGenerator.OUTPUT_TEMP_PATH, `${this.route.groupKey}.js`)
    if (util.isFile(filePath)) {
      return util.readFile(filePath)
    }
    return ''
  }

  _getSwaggerContent () {
    let content = {}
    const filePath = path.resolve(ApidocGenerator.OUTPUT_TEMP_PATH, `${this.route.groupKey}.json`)
    if (util.isFile(filePath)) {
      content = require(filePath)
    } else {
      content = {
        "swagger": "2.0",
        "info": {
          "description": ApidocGenerator.DESCRIPTION,
          "version": "1.0.0",
          "title": "Apidoc",
        },
        "host": ApidocGenerator.API_URL.replace('http://', ''),
        "basePath": "/",
        "tags": [],
        "paths": {},
        "schemes": ["http"]
      }
    }
    if(!util.toArray(content.tags, 'name').includes(this.route.group)) {
      content.tags.push({ name: this.route.group })
    }
    return content
  }

  _generateApidocJS (content) {
    const filePath = path.resolve(ApidocGenerator.OUTPUT_TEMP_PATH, `${this.route.groupKey}.js`)
    util.removeFile(filePath)
    util.writeFile(filePath, content)
    // console.log(' [creado] ', filePath.replace(process.cwd(), ''))
  }

  _generateSwagger (content) {
    const filePath = path.resolve(ApidocGenerator.OUTPUT_TEMP_PATH, `${this.route.groupKey}.json`)
    util.removeFile(filePath)
    util.writeFile(filePath, JSON.stringify(content, null, 2))
    // console.log(' [creado] ', filePath.replace(process.cwd(), ''))
  }
}

ApidocGenerator.SRC_PATH        = path.resolve(process.cwd(), `src`)
ApidocGenerator.BUILD_PATH      = path.resolve(process.cwd(), `build`)
ApidocGenerator.API_URL         = 'http://localhost:4000'
ApidocGenerator.REDIRECT_PATH   = '/apidoc'
ApidocGenerator.DOC_SERVER_PORT = 5000
ApidocGenerator.DESCRIPTION     = ''
ApidocGenerator.HELP_TYPE       = ''

function _getPropertiesExtra(route) {
  if (ApidocGenerator.HELP_TYPE === 'YAML') {
    return _getPropertiesFromYAML(route)
  }
  if (ApidocGenerator.HELP_TYPE === 'JSON') {
    return _getPropertiesFromJSON(route)
  }
  return {}
}

function _getPropertiesFromJSON(route) {
  const JSON_ROUTE_PATH = path.resolve(ApidocGenerator.SRC_PATH, `${route.groupKey}.json`)
  const routes = util.isFile(JSON_ROUTE_PATH) ? require(JSON_ROUTE_PATH) : {}
  let routeJSON = null
  routes.forEach(obj => { if (obj.method === route.method && obj.path === route.path) { routeJSON = obj } })
  return routeJSON
}

function _getPropertiesFromYAML(route) {
  const YAML_ROUTE_PATH = path.resolve(ApidocGenerator.SRC_PATH, `${route.groupKey}.yml`)
  const routes = YAML.load(YAML_ROUTE_PATH)
  let routeYAML = null
  routes.forEach(obj => { if (obj.method === route.method && obj.path === route.path) { routeYAML = obj } })
  return routeYAML
}

function _kebabToWords (text) {
  text = _.replace(text, /-/g, '/')
  const split = text.split('/')
  let words = ''
  split.forEach(e => {
    words += _.upperFirst(_.deburr(e)) + ' '
  })
  return words.trim()
}

function _getRoutes (app) {
  const routes = {}
  for (let i in app._router.stack) {
    const r = app._router.stack[i]
    if (r.route && r.route.path) {
      const routePath = r.route.path
      const method    = r.route.stack[0].method.toLowerCase()
      const route = new Route()
      route.setGroup('Default')
      route.setMethod(method)
      route.setPath(routePath)
      if (!routes[route.groupKey]) { routes[route.groupKey] = [] }
      routes[route.groupKey].push(route)
    } else {
      const urlBase = r.regexp.toString().split('/^').pop().split('/?').shift().replace(/\\/g, '')
      if (urlBase !== '') {
        for (let sr in r.handle.stack) {
          const routePath = urlBase + r.handle.stack[sr].route.path
          const method    = r.handle.stack[sr].route.stack[0].method.toLowerCase()
          const route = new Route()
          route.setGroup(urlBase)
          route.setMethod(method)
          route.setPath(routePath)
          if (!routes[route.groupKey]) { routes[route.groupKey] = [] }
          routes[route.groupKey].push(route)
        }
      }
    }
  }
  return routes
}

async function _build () {
  console.log('\n Uniendo ficheros ...\n')

  // APIDOC JS
  const filePath1 = path.resolve(ApidocGenerator.BUILD_PATH, `apidoc.js`)
  if(util.isFile(filePath1)) { util.removeFile(filePath1) }

  let content = ''
  util.find(path.resolve(ApidocGenerator.BUILD_PATH, 'routes'), '.js',  (info) => {
    const newBlock = util.readFile(info.filePath)
    const newRouteKey = newBlock.split('\n')[2]
    if (content.includes(newRouteKey)) {
      console.log(` \x1b[33mAdvertencia:\x1b[0m El servicio "${newRouteKey.substr(7)}" ya ha sido documentado. (ignorado)`)
      return
    }
    content += newBlock
  })
  util.writeFile(filePath1, content)
  console.log('', filePath1.replace(process.cwd(), ''), '\u2713')

  // SWAGGER
  const filePath2 = path.resolve(ApidocGenerator.BUILD_PATH, `swagger.json`)
  if(util.isFile(filePath2)) { util.removeFile(filePath2) }

  let content2 = null
  util.find(path.resolve(ApidocGenerator.BUILD_PATH, 'routes'), '.json',  (info) => {
    const contentToAdd = require(info.filePath)
    if (!content2) {
      content2 = contentToAdd
      return
    }
    if(!util.toArray(content2.tags, 'name').includes(contentToAdd.tags[0].name)) {
      content2.tags.push(contentToAdd.tags[0])
    }
    Object.keys(contentToAdd.paths).forEach(routePath => {
      content2.paths[routePath] = content2.paths[routePath] || {}
      Object.keys(contentToAdd.paths[routePath]).forEach(routeMethod => {
         if (content2.paths[routePath][routeMethod]) {
           const group = content2.paths[routePath][routeMethod].tags.toString()
           return
         }
         content2.paths[routePath][routeMethod] = contentToAdd.paths[routePath][[routeMethod]]
      })
    })
  })
  util.writeFile(filePath2, JSON.stringify(content2, null, 2))
  console.log('', filePath2.replace(process.cwd(), ''), '\u2713\n')
  await _buildApidocJS()
  const SWAGGER_JSON_OUTPUT_PATH = path.resolve(ApidocGenerator.BUILD_PATH, `public/swagger.json`)
  util.copyFile(path.resolve(ApidocGenerator.BUILD_PATH, `swagger.json`), SWAGGER_JSON_OUTPUT_PATH)
  console.log('', SWAGGER_JSON_OUTPUT_PATH.replace(process.cwd(), ''), '\u2713')
}

async function _buildApidocJS () {
  console.log(' Publicando ficheros ...\n');
  const templatePath        = path.resolve(__dirname, '../template/apidocjs')
  const templatePathSwagger = path.resolve(__dirname, '../template/swagger')
  const tmpPath             = path.resolve(__dirname, '../.temp')
  util.mkdir(tmpPath)
  const apidocConfig = {
    title     : 'Apidoc',
    name      : 'Documentación',
    version   : '1.0.0',
    url       : `${ApidocGenerator.API_URL}`,
    sampleUrl : `${ApidocGenerator.API_URL}`,
    template: {
      withGenerator : false,
      withCompare   : true,
      forceLanguage : 'es'
    },
    header: null,
    footer: null
  }
  util.writeFile(path.resolve(tmpPath, 'apidoc.json'), JSON.stringify(apidocConfig, null, 2))
  util.copyFile(path.resolve(ApidocGenerator.BUILD_PATH, `apidoc.js`), path.resolve(tmpPath, `apidoc.js`))
  const output = path.resolve(ApidocGenerator.BUILD_PATH, 'public/apidoc')
  console.log('', output.replace(process.cwd(), ''), '\u2713')
  const SWAGGER_OUTPUT_PATH = path.resolve(ApidocGenerator.BUILD_PATH, 'public/swagger')
  await util.copyDir(templatePathSwagger, SWAGGER_OUTPUT_PATH)
  console.log('', SWAGGER_OUTPUT_PATH.replace(process.cwd(), ''), '\u2713')
  try { util.rmdir(output) } catch (e) { }
  try {
    await _createApidoc('.', output, templatePath, tmpPath)
  } catch (e) { throw e }
  try { util.rmdir(tmpPath) } catch (e) { }
}

/**
* Ejecuta el comando para crear el APIDOC.
* @param {String} input    - Ruta del directorio de entrada.
* @param {String} output   - Ruta del directorio de salida.
* @param {String} template - Ruta del directorio del template.
* @param {String} execPath - Ruta desde donde se ejecutará el comando.
* @return {Promise}
*/
async function _createApidoc (input, output, template, execPath) {
  try {
    await util.cmd(`node "../../apidoc/bin/apidoc" -i "${input}" -o "${output}" -t "${template}"`, execPath)
  } catch (e) {
    try {
      await util.cmd(`node "../node_modules/apidoc/bin/apidoc" -i "${input}" -o "${output}" -t "${template}"`, execPath)
    } catch (e) {
      try {
        await util.cmd(`apidoc -i "${input}" -o "${output}" -t "${template}"`, execPath)
      } catch (e) {
        if (typeof e === 'string' && (e.includes('apidoc: not found') || e.includes('no se reconoce como un comando'))) {
          throw new Error(`ApidocJS no se encuentra instalado: Ejecute 'npm install -g apidoc' para solucionarlo.`)
        }
        throw e
      }
    }
  }
}

function _crearCarpeta (dirPath, options = {}) {
  if (util.isDir(dirPath) && (options.force === true)) {
    util.rmdir(dirPath)
  }
  if (!util.isDir(dirPath)) {
    util.mkdir(dirPath)
  }
}

function _crearArchivos (groupKey) {
  // Archivo JS
  let content = util.readFile(path.resolve(__dirname, '../template/route.js'))
  let filePath = path.resolve(ApidocGenerator.SRC_PATH,`${groupKey}.js`)
  if (!util.isFile(filePath)) {
    util.writeFile(filePath, content)
    console.log(`\n ${filePath.replace(process.cwd(), '')} \u2713`)
  }
  // Archivo JSON
  if (ApidocGenerator.HELP_TYPE === 'JSON') {
    content = []
    let jsonPath = path.resolve(ApidocGenerator.SRC_PATH,`${groupKey}.json`)
    if (!util.isFile(jsonPath)) {
      util.writeFile(jsonPath, JSON.stringify(content, null, 2) + '\n')
      console.log(` ${jsonPath.replace(process.cwd(), '')} \u2713\n`)
    }
  }
  // Archivo YAML
  if (ApidocGenerator.HELP_TYPE === 'YAML') {
    content = []
    let jsonPath = path.resolve(ApidocGenerator.SRC_PATH,`${groupKey}.yml`)
    if (!util.isFile(jsonPath)) {
      util.writeFile(jsonPath, '---\n' + YAML.stringify(content, 4) + '\n')
      console.log(` ${jsonPath.replace(process.cwd(), '')} \u2713\n`)
    }
  }
}

function _crearContenido (groupKey, route) {
  // Archivo JS
  let filePath = path.resolve(ApidocGenerator.SRC_PATH,`${groupKey}.js`)
  let content = util.readFile(filePath)
  const ROUTE_FLAG = '// <!-- [ROUTE DEFINITION] --!> //'
  if (!content.includes(`await ApiGen.${route.method}('${route.path}')`)) {
    content = content.replace(ROUTE_FLAG, `await ApiGen.${route.method}('${route.path}').generate()\n\n  ${ROUTE_FLAG}`)
    console.log(`      [${groupKey}] - [${route.method}] ${route.path} \u2713`)
  }
  util.removeFile(filePath)
  util.writeFile(filePath, content)

  const existeRuta = (routes, routeMethod, routePath) => {
    for (let i in routes) {
      if (routes[i].method === routeMethod && routes[i].path === routePath) { return true }
    }
    return false
  }

  //  Archivo JSON
  if (ApidocGenerator.HELP_TYPE === 'JSON') {
    filePath = path.resolve(ApidocGenerator.SRC_PATH,`${groupKey}.json`)
    const JSON_STRING = util.isFile(filePath) ? util.readFile(filePath) || '[]' : '[]'
    content = JSON.parse(JSON_STRING)
    if (!existeRuta(content, route.method, route.path)) {
      content.push({ method: route.method, path: route.path })
    }
    util.removeFile(filePath)
    util.writeFile(filePath, JSON.stringify(content, null, 2) + '\n')
  }

  // Archivo YAML
  if (ApidocGenerator.HELP_TYPE === 'YAML') {
    filePath = path.resolve(ApidocGenerator.SRC_PATH,`${groupKey}.yml`)
    const YAML_STRING = util.isFile(filePath) ? util.readFile(filePath) || '---\n' : '---\n'
    content = YAML.parse(YAML_STRING)
    if (!existeRuta(content, route.method, route.path)) {
      content.push({ method: route.method, path: route.path })
    }
    util.removeFile(filePath)
    util.writeFile(filePath, YAML.stringify(content, 4) + '\n')
  }
}

function _verificarRutasDelArchivo (filePath, ROUTES) {
  const existInRoutes = (method, path) => {
    for (let i in ROUTES) {
      const routes = ROUTES[i]
      for(let i in routes) {
        const route = routes[i]
        if (route.method === method && route.path === path) {
          return true
        }
      }
    }
    return false
  }
  const content = util.readFile(filePath).split('\n')
  for (let i in content) {
    const line = content[i]
    if (line.includes('await ApiGen.')) {
      const A = line.indexOf('await ApiGen.') + 13
      const B = line.indexOf('(', A)
      const routeMethod = line.substring(A, B)
      const C = line.indexOf(')', B + 2)
      const routePath =line.substring(B + 2, C - 1)
      if (!existInRoutes(routeMethod, routePath)) {
        const fileName = filePath.replace(process.cwd(), '')
        const groupKey = filePath.split(path.sep).pop().replace('.js', '')
        console.log(`      \x1b[33m[${groupKey}] - [${routeMethod}] ${routePath} NO EXISTE EN LA APLICACIÓN \x1b[0m (${fileName}:${parseInt(i) + 1}:${A - 13 + 1})`)
      }
    }
  }
}

module.exports = ApidocGenerator
