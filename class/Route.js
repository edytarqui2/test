const _       = require('lodash')
const request = require('request')

const FieldCreator = require('./FieldCreator')

class Route {
  constructor () {
    this.method      = 'get'
    this.path        = '/'       // Ej.: /api/v1/users/:id
    this.group       = 'Default' // Ej.: Api V1 Users
    this.groupKey    = 'default' // Ej.: api-v1-users
    this.version     = 1
    this.name        = null      // Ej.: Devuelve una lista de usuarios
    this.key         = null      // Ej.: admin
    this.description = null
    this.permissions = null      // Ej.: ['admin', 'user']
    this.input       = null      // FieldGroup
    this.output      = null      // FieldGroup
    this.inputData   = null      // Datos de entrada obj = { headers, params, query, body }
    this.outputData  = null      // Datos de salida obj = body
    this.request     = false
  }

  setName (name) {
    this.name = name
  }

  setGroup (groupName) {
    this.group    = _toWords(groupName)
    this.groupKey = _toKebab(this.group)
  }

  setVersion (version) {
    this.version = version
  }

  setKey (key) {
    this.key = key
  }

  setDescription (description) {
    this.description = description
  }

  setPersmissions (persmissions) {
    this.persmissions = persmissions
  }

  setMethod (method) {
    this.method = method
  }

  setPath (routePath) {
    this.path = routePath
  }

  setInputData (inputData) {
    this.inputData = inputData
  }

  setOutputData (outputData) {
    this.outputData = outputData
  }

  setInput (input) {
    this.input = input
  }

  setOutput (output) {
    this.output = output
  }

  setRequest (value) {
    this.request = value
  }

  async updateProperties (options = {}) {
    const PROPS = options.properties || {}
    this.key            = this.key            || PROPS.key
    this.permissions    = this.permissions    || PROPS.permissions
    this.version        = this.version        || PROPS.version
    this.request        = this.request        || PROPS.request
    this.name           = this.name           || PROPS.name        || `[${this.method}] ${_toWords(this.path)}${this.key ? ` (${this.key})` : ''}`
    this.description    = this.description    || PROPS.description || this.name
    this.inputData      = this.inputData      || PROPS.inputData
    this.outputData     = this.outputData     || PROPS.outputData
    this.inputExamples  = this.inputExamples  || PROPS.inputExamples
    this.outputExamples = this.outputExamples || PROPS.outputExamples
    if (this.request === true) {
      this.outputData = this.outputData || await _request(this, options)
    }
    this.input  = this.input  || FieldCreator.groupObj(this.inputData)  || {}
    this.output = this.output || FieldCreator.groupObj(this.outputData) || {}
  }

  swaggerPath () {
    const split = this.path.split('/')
    const path2 = []
    let pathSwagger = this.path
    split.forEach(prop => {
      path2.push(prop.startsWith(':') ? `{${prop.substr(1)}}` : prop)
    })
    const swaggerPath = `/${_.trim(path2.join('/'), '/')}${this.path.endsWith('/') ? '/' : ''}`
    return swaggerPath
  }

  requestPath (inputDataParams = {}) {
    let requestPath = this.path
    Object.keys(inputDataParams).forEach(prop => {
      requestPath = requestPath.replace(`:${prop}`, `${inputDataParams[prop]}`)
    })
    return requestPath
  }
}

function _toWords (text) {
  const split = text.split('/')
  let words = ''
  split.forEach(e => {
    words += _.upperFirst(_.deburr(e)) + ' '
  })
  return words.trim()
}

function _toKebab (text) {
  const kebab = _.trim(text.toLowerCase().split(' ').join('-'), '-')
  return kebab.trim()
}

function _request (route, config) {
  const inputDataParams = route.inputData ? route.inputData.params : {}
  const options = { method: route.method.toUpperCase(), uri: `${config.apiUrl}${route.requestPath(inputDataParams)}` }
  if (route.inputData && route.inputData.body)    { options.json    = route.inputData.body }
  if (route.inputData && route.inputData.headers) { options.headers = route.inputData.headers }
  return new Promise((resolve, reject) => {
    return request(options, (error, response, body) => {
      if (error) {
        if (error.code === 'ECONNREFUSED') {
          console.log(' \x1b[31mError:\x1b[0m El servicio a documentar no se encuentra disponible :(\n')
        }
        return reject(error)
      }
      const CODE   = response.statusCode
      const STATUS = response.statusMessage
      try { if (typeof body === 'string') body = JSON.parse(body) } catch (e) {}
      if (CODE >= 200 && CODE < 300) { return resolve(body) }
      console.log(`\n \x1b[31mError:\x1b[0m [${route.method}] ${route.path}\n`)
      console.log('DATA:')
      console.log(route.inputData)
      console.log()
      console.log('REQUEST:')
      console.log(options)
      console.log()
      console.log(`RESPONSE (${CODE} ${STATUS}):`)
      console.log(body)
      console.log()
      return reject(new Error(`La petición ha finalizado con un error.`))
    })
  })
}

module.exports = Route
